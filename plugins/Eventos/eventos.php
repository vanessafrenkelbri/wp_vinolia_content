<?php
/**
 * Plugin Name: Eventos Vinolia
 * Plugin URI: 
 * Description: Crea eventos de vinolia
 * Version: 1.0.0
 * Author: Colibri
 * Author URI: http://www.bri.cl

 */
defined( 'ABSPATH' ) or die( '¡Sin trampas!' );


// =============================================================
//       Define Post-type Eventos
// =============================================================

function custom_post_eventos() {
  $labels = array(
    'name'               => _x( 'Eventos', 'post type general name' ),
    'singular_name'      => _x( 'Evento', 'post type singular name' ),
    'add_new'            => _x( 'Agreagar Nuevo ', 'Evento' ),
    'add_new_item'       => __( 'Agregar Nuevo Evento' ),
    'edit_item'          => __( 'Editar Evento' ),
    'new_item'           => __( 'Nuevo Evento' ),
    'all_items'          => __( 'Todos los Eventos' ),
    'view_item'          => __( 'Ver Eventos' ),
    'search_items'       => __( 'Buscar Eventos' ),
    'not_found'          => __( 'No se encontraron Eventos' ),
    'not_found_in_trash' => __( 'No se encontraron eventos en la papelera' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Eventos Vinolia'
  );

  // $icon = plugin_dir_path( __FILE__ ) .'icon-plugin.png';

  $args = array(
    'labels'        => $labels,
    'description'   => 'Eventos de Vinolia',
    'public'        => true,
    'menu_position' => 3,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'has_archive'   => true,
    'menu_icon'     => 'dashicons-tag',
    
  );
  register_post_type( 'eventos', $args ); 
}
add_action( 'init', 'custom_post_eventos' );




// =============================================================
//       Define taxonomia prodcutos
// =============================================================

function evento_taxonomy() {  

    // Add new "Locations" taxonomy to Posts
  register_taxonomy('tipo_evento', 'evento', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    // This array of options controls the labels displayed in the WordPress Admin UI
      'labels' => array(
      'name' => _x( 'Tipo de evento', 'taxonomy general name' ),
      'singular_name' => _x( 'evento', 'taxonomy singular name' ),
      'search_items' =>  __( 'Buscar Tipos de eventos' ),
      'all_items' => __( 'Todo los tipo de eventos' ),
      'parent_item' => __( 'Tipo de evento padre' ),
      'parent_item_colon' => __( 'padre tipo de evento' ),
      'edit_item' => __( 'Editar tipo de evento' ),
      'update_item' => __( 'Actualizar tipo de evento' ),
      'add_new_item' => __( 'Agregar nuevo tipo de evento' ),
      'new_item_name' => __( 'Agregar nombre de nuevo tipo de evento' ),
      'menu_name' => __( 'Tipo de evento' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
    'slug' => 'tipo_evento', // This controls the base slug that will display before each term
    'with_front' => true, // Don't display the category base before "/tipo/"
    'hierarchical' => false // This will allow URL's like "/tipo/boston/cambridge/"
    ),
  ));
}
add_action( 'init', 'evento_taxonomy', 0 );





