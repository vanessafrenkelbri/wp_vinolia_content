<?php
function my_theme_enqueue_styles() {
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version'));
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/bootstrap.min.css', array( 'js_composer_front' ));
    wp_enqueue_style( 'slick', get_stylesheet_directory_uri() . '/assets/slick.css', array( 'bootstrap' ));
    wp_enqueue_style( 'slick-theme', get_stylesheet_directory_uri() . '/assets/slick-theme.css', array( 'slick' ));
    wp_enqueue_style( 'datepicker', get_stylesheet_directory_uri() . '/assets/datepicker.min.css', array( 'slick-theme' ));
    wp_enqueue_style( 'vinolia-style', get_stylesheet_directory_uri() . '/css/vinolia.css', array( 'datepicker' ));

    //scripts
    //wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), '', true );
    wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/assets/bootstrap.bundle.min.js', array('jquery'), true );
    wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/assets/slick.min.js', array('jquery'), true );
    wp_enqueue_script( 'datepicker', get_stylesheet_directory_uri() . '/assets/datepicker.min.js', array('slick'), true );
    wp_enqueue_script( 'datepicker-en', get_stylesheet_directory_uri() . '/assets/i18n/datepicker.en.js', array('datepicker'), true );
    wp_enqueue_script( 'datepicker-es', get_stylesheet_directory_uri() . '/assets/i18n/datepicker.es.js', array('datepicker'), true );
    wp_enqueue_script( 'datepicker-pt', get_stylesheet_directory_uri() . '/assets/i18n/datepicker.pt-BR.js', array('datepicker'), true );



    wp_enqueue_script( 'init', get_stylesheet_directory_uri() . '/js/init.js', array('datepicker'), true );



}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function wpdocs_theme_setup() {
//add_image_size( '', 300 ); // 300 pixels wide (and unlimited height)
add_image_size( '300x300', 300, 300, true ); // (cropped)
add_image_size( '400x300', 400, 300, true ); // (cropped)

}
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );

function shortcode_calendar_form() {
    $url_image_calendar = get_stylesheet_directory_uri() . '/images/icono-icon-calendar.png';
    $url_image_people = get_stylesheet_directory_uri() . '/images/icono-icon-people.png';

    $myForm = 
    "<form action='POST' class='calendar-form-body' id='home-calendar'>
    <div class='form-row'>
        <div class='col-md-4'>
            <div class='input-group'>
                <label for='day' class='sr-only'>¿Qué día quieres venir?</label>
                <input type='text' class='datepicker-here form-control' data-position='right top' data-language='es' placeholder='¿Qué día quieres venir?'/>
                <div class='input-group-append'>
                    <span class='input-group-text' id=''><img src='$url_image_calendar'></span>
                </div>
            </div>
        </div>
        <div class='col-md-4'>
            <div class='input-group'>
                <label for='people' class='sr-only'>¿Cuántas personas?</label>
                <input type='number' class='form-control' id='people' placeholder='¿Cuántas personas?'>
                <div class='input-group-append'>
                    <span class='input-group-text' id=''><img src='$url_image_people'></span>
                </div>
            </div>
        </div>
        <div class='col-md-4'>
            <input type='submit' value='Buscar' class='btn btn-yellow float-right'>
        </div>
    </div>

</form>";
echo $myForm;
}


add_shortcode('calendar_home', 'shortcode_calendar_form');