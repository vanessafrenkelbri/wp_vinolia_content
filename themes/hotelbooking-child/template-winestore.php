<?php
/* Template Name: winestore
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BetterOne
 */
get_header(); 
?>
	<?php 
    //get header metabox
$nd_options_meta_box_page_header_img = get_post_meta( get_the_ID(), 'nd_options_meta_box_page_header_img', true );
$nd_options_meta_box_page_header_img_title = get_post_meta( get_the_ID(), 'nd_options_meta_box_page_header_img_title', true );
$nd_options_meta_box_page_header_img_position = get_post_meta( get_the_ID(), 'nd_options_meta_box_page_header_img_position', true );



if ( $nd_options_meta_box_page_header_img != '' ) { ?>	


	<div id="nd_options_page_header_img_layout_5" class="nd_options_section nd_options_background_size_cover <?php echo $nd_options_meta_box_page_header_img_position ?>" style="background-image:url(<?php echo $nd_options_meta_box_page_header_img; ?>);">

        <div class="nd_options_section nd_options_bg_greydark_alpha_3">

            <!--start nd_options_container-->
            <div class="nd_options_container nd_options_clearfix">


                <div id="nd_options_page_header_image_space_top" class="nd_options_section nd_options_height_110"></div>

                <div class="nd_options_section nd_options_padding_15 nd_options_box_sizing_border_box nd_options_text_align_center">

                    <h1 class="nd_options_color_white nd_options_font_size_55 nd_options_font_size_40_all_iphone nd_options_line_height_40_all_iphone nd_options_first_font"><?php echo $nd_options_meta_box_page_header_img_title; ?></h1>

                </div>

                <div id="nd_options_page_header_image_space_bottom" class="nd_options_section nd_options_height_110"></div>                

            </div>
            <!--end container-->

        </div>

    </div>


<?php } ?>

<div class="custom-content clearfix">
<section id="our-restorant">
		<div class="container-full">
				<div class="col-left img-bg" style="background-image:url('http://placehold.it/600x300')">
				</div>
				<div class="col-right">
				<div class="block">
				<span class="title-upper title-underline">Gastronomía</span>
				<h1 class="title-serif text-center ">Nuestro Restaurant</h1>
				<p>En Vinolia tenemos todo preparado para que vengas a disfrutar una atractiva variedad de platos y picoteos en nuestro restaurant junto a la mejor selección de vinos y cepas de las viñas nacionales más destacadas. Además, en nuestro Winestore y emporio podrás llevarte a casa tus vinos favoritos y otras delicias para maridar. </p>
			
				</div>


				</div>

			</div>

	</section>
	<section id="banner-menu">
		<div class="container">
			<h6>Cada día preparamos un delicioso menu para que disfrutes y te relajes en el grato y acogedor ambiente de Vinolia</h6>
			<a href="#" class="btn btn-yellow">Conoce nuestra carta</a>
		</div>
	</section>
	<section id="executive-menu" >
		<div class="container">
			<div class="row no-gutters">
				<div class="col-lg-4">
					<a href="#">
						<img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/menu.png" alt="" class="img-fluid">

					</a>
				</div>
				<div class="col-lg-8">
					<img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/restaurant_foto2.jpg" alt="" class="img-fluid">
					<p>Reserva tu mesa y ven a disfrutar en el corazón de uno de los barrios más vibrantes y exclusivos de Santiago </p>
				</div>
			</div>
		</div>
		<img src="http://placehold.it/800x300" alt="" class="img-100" style="width:100%;">
	</section>
	<section id="our-store">
		<div class="container">
			<div class="row no-grutters align-items-center">
			<div class="col-lg-6">
				<span class="title-upper title-underline">TIENDA</span>
				<h1 class="title-serif text-center ">WINESTORE VINOLIA</h1>
				<p>Elige tus vinos favoritos y atrévete a descubrir la gran variedad que tenemos a tu disposición para disfrutar en la comodidad de tu hogar, hacer un regalo especial, maridarlos con las delicatessen de nuestro emporio o incluso elegir un vino para disfrutar en nuestro restaurant:</p>
				<p>¡El descorche es sin costo! </p>
			</div>
			<div class="col-lg-6">
				<img src="http://placehold.it/600x600" alt="" class="img-100" style="width:100%;">
			</div>
			</div>
			<div class="yellow-arrow-banner">
                <h4 class="yellow-arrow-banner-title">Descorche sin costo adicional</h4>
                <p class="yellow-arrow-banner-p">Todos los vinos que compres en nuestro WineStore puedes disfrutarlos en nuestro renovado WineBar, en el Restaurante junto a nuestra nueva carta o   la terraza de Vinolia sin costo adicional por el descorchado.</p>
                <a href="#" class="btn btn-outline-yellow">Ver más promociones</a>
            </div>
		</div>
	</section>
	<section id="feature-wineyard">
		<div class="container-full">
			<div class="fetured-post">
				<div class="row no-gutters">
						<div class="col-lg-4">
							<img src="http://placehold.it/600x600" alt="" style="width:100%" class="img-fluid">
						</div>
						<div class="col-lg-8">
							<div class="post-block">
								<span class="title-upper title-underline">Viña destacada</span>
								<h1 class="title-serif text-center ">Santa Rita</h1>
								<p class="excerpt">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam, beatae vero? Vero at quod nemo esse reiciendis ipsa corrupti necessitatibus iure hic ea, omnis delectus alias velit, quidem quasi deleniti!</p>
								<a href="" class="btn btn-yellow"> Ver más promociones</a>
							</div>
						</div>
				</div>
			</div>

		</div>
	</section>
	<section id="promocion">
		<div class="cajita-1"></div>
		<div class="cajita-2"></div>
	</section>




</div>
<?php 
get_footer(); ?>