module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),



        pug: {
            compile: {
              options: {
                data: {
                  debug: false
                },
                pretty: true
              },
              files: {
                // 'index.html': ['_dev/pug/home.pug'],
                // 'page-default.html': ['_dev/pug/page-interior-side-menu.pug'],
                // 'page-interiror.html': ['_dev/pug/page-interior.pug'],

                

              }
            }
          },        

        sass: {
            dist: {
                options: {
                    //style: 'compressed'
                    style: 'expanded'
                },
                files: {
                    // Nuestro Sass es compilado a nuestro archivo CSS
                    'css/vinolia.css': '_dev/scss/vinolia.scss'
                    
                }
            }
        },

        postcss: {
            options: {
              map: true, // inline sourcemaps
         
              // or
              map: {
                  inline: false, // save all sourcemaps as separate files...
                  annotation: 'css/maps/' // ...to the specified directory
              },
         
              processors: [
                require('pixrem')(), // add fallbacks for rem units
                require('autoprefixer')({Browserslist: 'last 2 versions'}), // add vendor prefixes
                require('cssnano')() // minify the result
              ]
            },
            dist: {
              src: 'css/*.css'
            }
          },
      

        watch: {
            site: {
                // Vigilamos cualquier cambio en nuestros archivos
                files: ['_dev/scss/**/*.scss', 'css/*.css', '_dev/js/**/*.js', '*.html' ,'js/**/*.js', '_dev/pug/**/*.pug', '*.php'],
                tasks: ['default']
            },
            options: {
                // Instalamos la extensión de Livereload en Chrome para ver cambios
                // automáticos en el navegador sin hacer refresh
                spawn: false,
                livereload: true
            }
        }

    });

    // Cargamos los plugins

    grunt.loadNpmTasks('grunt-contrib-pug');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');


    // Registrar tareas
    //grunt.registerTask('default', ['pug', 'sass', 'watch']);
    //grunt.registerTask('default', ['concat', 'uglify','pug', 'sass', 'watch']);
    //grunt.registerTask('default', ['concat', 'uglify','pug', 'sass', 'scp', 'watch']);
    grunt.registerTask('default', ['pug', 'sass', 'postcss','watch']);    
    //grunt.registerTask('default', [ 'scp' ]);
    
    
}