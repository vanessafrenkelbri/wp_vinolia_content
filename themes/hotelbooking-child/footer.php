<footer>
<div id="top-bar-on-footer"class="only-movil">

</div>
<div class="footer-top">
    <?php  $nicdark_customizer_logo_img = get_option( 'nicdark_customizer_logo_img' );?>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="footer-widget">
                <h6 class="footer-title">Horario de Atención</h6>
                <p>Lunes a Sábado de 11:00 a 23:00 Hrs</p>
            </div>
            <div class="footer-widget">
                <h6 class="footer-title">Contacáctenos</h6>
                <p>+562 2604 8528</p>
                <p><a href="mailto:contacto@vinolia.cl">contacto@vinolia.cl</a></p>
            </div>
            <div class="footer-widget">
                <h6 class="footer-title">Visítenos</h6>
                <p>+Alonso de Monroy 2869, Local 5, vITACURA</p>
                <p><a href="mailto:contacto@vinolia.cl">contacto@vinolia.cl</a></p>
            </div>

        </div>
        <div class="col-md-6">
            <div class="widget-newsletter">
                <p>Suscríbete a nuestra Newsletter para recibir descuentos y novedades</p>
                <form action="POST" class="form-input">
                    <div class="input-group">
                     <input type="email" class="form-control" placeholder="" aria-label="" aria-describedby="">
                     <div class="input-group-append">
                        <button class="btn btn-yellow" type="button" id="button-addon2">Suscribir</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-3">
            <div class="footer-widget">
                <h6 class="footer-title">IMPORTANTE</h6>
                <p>Disfrute de la experiencia, beba con moderación.<br>Si va a conducir no beba y si va a beber pase las llaves.</p>
            </div>
        </div>
 
    </div>
</div>


</footer>



<?php wp_footer(); ?>

	
</body>  
</html>