<?php
/* Template Name: home
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BetterOne
 */
get_header(); 
?>
<section id="home-main-banner" style="background-image:url('http://localhost/wp_vinolia_2/wp-content/uploads/revslider/home-vinolia-alternativo/vinolia.png')">
    <div class="title-block">
        <h1 class="title-section text-center">Bienvenidos a una experiencia <br> única de degustación de vinos en América</h1>
    </div>
    <div class="calendar-form-block">
        <div class="calendar-form-header">
            <img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-movil.png" alt="vinolia logo">
            <h2 class="block-title">Programa tu degustación de vinos</h2>
        </div>
        <?php echo do_shortcode( '[calendar_home]' ); ?>
        <div class="calendar-form-footeer">
            <p>Estamos ubicados en Alonso de Monroy 2869, Local 5 Vitacura</p>
        </div>
    </div>
</section>
<section id="home-about-us" >
    <div class="container expanded">
        <div class="descalce-box">
            <div class="black-box">
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <div class="section-title">
                            <p class="title-upper title-underline white">Vinolia</p>
                            <h1 class="title-serif white text-center ">La aventura del Vino en Santiago</h1>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p>Vinolia es el primer centro enoturístico urbano de América Latina, en el que podrás vivir una experiencia única en Santiago: un viaje multisensorial en el que podrás sentir, conocer y aprender sobre el maravilloso mundo vitivinícola de nuestro país.  </p>
                        <div class="btn-row">
                            <a href="#" class="btn btn-outline-yellow">Valle Casablanca</a>
                            <a href="#" class="btn btn-outline-yellow">Valle de Maipo</a>
                            <a href="#" class="btn btn-outline-yellow">Valle de Colchagua</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="yellow-box yellow-carrusel">

            <?php 
                    
                    $args = array( 
                        'category_name' =>  'news'

                    );


                    $the_query = new WP_Query( $args );
                    // The Loop
                    if ( $the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                <div class="expereincia">
                    <div class="block-1" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?> ')">
                        <h2>Cata<br>&<br>Maridaje</h2>
                    </div>
                    <div class="block-2">
                        <div class="post-content">
                            <h3 class="block-post-title"><?php the_title(); ?></h3>
                            <p class="block-post-excerpet"><?php the_excerpt(); ?></p>
                            <div class="actions-elements row">
                                <div class="col-md-6">
                                    <p>
                                        Sábado 13 de Julio | 20:00 hrs. <br>
                                        Lugar: Vinolia <br>
                                        $30.000* pp
                                    </p>
                                    <p>Cupos Limitados!</p>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" class="btn btn-outline-dark"> Reserva ahora</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            <?php endwhile;
            endif;
            // Reset Post Data
            wp_reset_postdata();
            ?>



            </div>
            <div class="yellow-arrow-banner">
                <h4 class="yellow-arrow-banner-title">Descorche sin costo adicional</h4>
                <p class="yellow-arrow-banner-p">Todos los vinos que compres en nuestro WineStore puedes disfrutarlos en nuestro renovado WineBar, en el Restaurante junto a nuestra nueva carta o   la terraza de Vinolia sin costo adicional por el descorchado.</p>
                <a href="#" class="btn btn-outline-yellow">Ver más promociones</a>
            </div>
        </div>


    </div>
</section>
<section id="home-associated-vineyards">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4">
                <div class="section-title">
                    <p class="title-upper title-underline">Variedades</p>
                    <h1 class="title-serif text-center ">Viñas Asosciadas</h1>
                </div>
            </div>
            <div class="col-md-8">
                <ul class="associated-vineyards-logos-list">
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-santa-rita.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-vina-el-principal.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-veramonte.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-viu-manent.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-perez-cruz.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-oc-wines.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-morande.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-montes.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-los-vascos.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-loma-larga.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-las-veletas.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-lapostolle.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-de-marino.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-dagaz.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-causino-macul.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-casas-del-bosque.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-casa-bauza.png" alt="" class="associated-vineyards-logo"></li>
                    <li><img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/logo-partner-bodegas-re.png" alt="" class="associated-vineyards-logo"></li>





                </ul>
            </div>

        </div>
    </div>
</section>
<section id="home-costumers-review" >
    <img src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/bg_header_eventos.jpg" alt="" class="home-costumers-img">
    <div class="black-reviews">
        <div class="container">
            <h4 class="title-serif white text-center tripadvisor-after">Ellos ya conocieron la experiecia vinolia</h4>
            <ul class="reviews reviews-carrusel">
                <li class="review">
                    <p>He ido a catas, en diferentes países, pero esta cata educacional te deja una maravillosa sensación.</p>
                    <span>IRVALAIRVALAIV</span>
                    <span>7/09/2019</span>
                </li>
                <li class="review">
                    <p>He ido a catas, en diferentes países, pero esta cata educacional te deja una maravillosa sensación.</p>
                    <span>IRVALAIRVALAIV</span>
                    <span>7/09/2019</span>
                </li>
                <li class="review">
                    <p>He ido a catas, en diferentes países, pero esta cata educacional te deja una maravillosa sensación.</p>
                    <span>IRVALAIRVALAIV</span>
                    <span>7/09/2019</span>
                </li>
            </ul>
        </div>

    </div>

</section>
<section id="home-partners">
    <div class="container">
        <h4 class="title-upper text-center title-underline">Nuestras alianzas</h4>
        <ul class="alianzas-grid">
            <li class=""><img src="http://placehold.it/300x150" alt="" class="alianzas-logos"></li>
            <li class=""><img src="http://placehold.it/300x150" alt="" class="alianzas-logos"></li>
            <li class=""><img src="http://placehold.it/300x150" alt="" class="alianzas-logos"></li>
            <li class=""><img src="http://placehold.it/300x150" alt="" class="alianzas-logos"></li>
            <li class=""><img src="http://placehold.it/300x150" alt="" class="alianzas-logos"></li>
            <li class=""><img src="http://placehold.it/300x150" alt="" class="alianzas-logos"></li>
            <li class=""><img src="http://placehold.it/300x150" alt="" class="alianzas-logos"></li>


        </ul>

    </div>
</section>
<section id="home-news">
    <div class="container">
        <span class="title-upper title-underline">Actividades y eventos</span>
        <h1 class="title-serif text-center ">Noticias Vinolia</h1>
        <div class="row news-row">

        <?php 
        
            $args = array( 
                'category_name' =>  'news',
                'posts_per_page' => 3,      
            );


            $the_query = new WP_Query( $args );
            // The Loop
            if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                        <div class="col-lg-4">
                            <div class="card">

                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <?php the_post_thumbnail('400x300', array( 'class' => "card-img-top")); ?>
                            </a>
                                <div class="card-body">
                                    <span class="card-news-caption">Noticia</span>
                                    <h5 class="card-news-title"><?php the_title ?></h5>
                                    <p class="card-text"><?php the_excerpt ?></p>
                                    <a href="<?php the_permalink(); ?>"class="btn btn-yellow">Ver más</a>
                                    
                                </div>

                            </div>
                        </div>



            <?php endwhile;
            endif;
            // Reset Post Data
            wp_reset_postdata();
            ?>


        </div>
    </div>

</section>
<section id="home-information">
    <div class="container">
    <span class="title-upper title-underline">Informaciones</span>
        <h1 class="title-serif text-center ">Espacios únicos</h1>
        <ul class="grid-services">
            <li>
                <img class="service-icon" src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/icono-terraza.svg" alt="" width="150" height="150">
                <h6 class="service-title">Lorem ipsum</h6>
                <p class="service-excerpt">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
            </li>
            <li>
                <img class="service-icon" src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/icono-terraza.svg" alt="" width="150" height="150">
                <h6 class="service-title">Lorem ipsum</h6>
                <p class="service-excerpt">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
            </li>
            <li>
                <img class="service-icon" src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/icono-terraza.svg" alt="" width="150" height="150">
                <h6 class="service-title">Lorem ipsum</h6>
                <p class="service-excerpt">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
            </li>
            <li>
                <img class="service-icon" src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/icono-terraza.svg" alt="" width="150" height="150">
                <h6 class="service-title">Lorem ipsum</h6>
                <p class="service-excerpt">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
            </li>
            <li>
                <img class="service-icon" src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/icono-terraza.svg" alt="" width="150" height="150">
                <h6 class="service-title">Lorem ipsum</h6>
                <p class="service-excerpt">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
            </li>
            <li>
                <img class="service-icon" src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/icono-terraza.svg" alt="" width="150" height="150">
                <h6 class="service-title">Lorem ipsum</h6>
                <p class="service-excerpt">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
            </li>
            <li>
                <img class="service-icon" src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/icono-terraza.svg" alt="" width="150" height="150">
                <h6 class="service-title">Lorem ipsum</h6>
                <p class="service-excerpt">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
            </li>
            <li>
                <img class="service-icon" src="http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/icono-terraza.svg" alt="" width="150" height="150">
                <h6 class="service-title">Lorem ipsum</h6>
                <p class="service-excerpt">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
            </li>



        </ul>
    </div>

</section>
<section id="home-events">
    <div class="bg-image" style="background-image:url('http://localhost/wp_vinolia_2/wp-content/uploads/2019/07/bg_header_promos.jpg')"></div>
    <div class="yellow-block">
        <div class="text-block">
            <h5>¿Tienes un evento programado?</h5>
            <p>Contáctanos para poder ofrecerte la mejor experiencia en vinos de Chile.</p>
            <a href="#" class="btn btn-dark">Formulario de contacto</a>
        </div>

    </div>
</section>
<?php 
get_footer(); ?>