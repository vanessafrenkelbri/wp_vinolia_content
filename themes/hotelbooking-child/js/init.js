jQuery(document).ready(function($){
    $('.yellow-carrusel').slick({
        arrows: false,
        autoplay:true,
        dots:true
    });
    $('.reviews-carrusel').slick({
        autoplay:true,   
        dots:true,
        arrows: false,
    });
    $( "#nd_options_navigation_2_top_header > .nd_options_container" ).clone().appendTo( "#top-bar-on-footer" );
});